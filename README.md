# Network traffic simulators

[![Platform](https://img.shields.io/badge/platform-Linux,%20OS%20X,%20Windows-green.svg?style=flat)](https://bitbucket.org/nikolskydn/traffic-simulators)
[![License](https://img.shields.io/badge/license-MIT-yellow.svg?style=flat)](https://opensource.org/licenses/mit-license.php)


## Brief

The complex was builded for scientific research. 
It is designed to create new efficient network traffic analysers.

Consists of four applications at the moment:

 1. *Iface* is designed to configure network settings;

 2. *Simulator* can add and tuning new program for generate network flows;

 3. *Experiment*  run simulators and perform to dump traffic.
 
 4. *Statistic*  performs detection and recognition of anomalies in traffic.

After setting up the network and simulators, 
can simulate the process of transmitting abnormal traffic (see figure 1).
Next, copy the dump files from the remote computers to the local host 
(in `media` directory) (see figure 2). 

![Figure 1: Run experiment](docs/simulator_control/run_experiment.png)

![Figure 2: Copy remote file](docs/simulator_control/copy_remote_file.png)

For recognition, create different files with statistics 
(see figure 3 and figure 4).

![Figure 3: Make statistic](docs/statistics/sc_make_statistic.png)

![Figure 4: View statistic](docs/statistics/sc_view_statistic.png)

After the statistics are generated, perform the analysis
(see figure 5 and figure 6).

![Figure 5: Analyze](docs/statistics/sc_analyze.png)

![Figure 6: View analize](docs/statistics/sc_view_analize.png)



*The project is in the state of development.*
