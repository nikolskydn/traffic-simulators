from django.contrib import admin
from analysis.models import *


class ResearchAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
admin.site.register(Research,ResearchAdmin)

class StatisticAdmin(admin.ModelAdmin):
        list_display = ('research_name','features','window','strategy',
                        'history')
        list_filter = ('research_name','strategy','window','features')
        actions = ('make_statistics','view_statistics')
admin.site.register(Statistic,StatisticAdmin)
admin.site.add_action(make_statistics)
admin.site.add_action(view_statistics)
admin.site.disable_action('make_statistics')
admin.site.disable_action('view_statistics')

class RecognitionAdmin(admin.ModelAdmin):
    list_display = ('unknown','background','get_patterns','strategy','window','history')
    actions = ('analyze','view_analyzes')
    def get_patterns(self,obj):
        return '\n'.join(p.statistic.rsplit('/')[-1].replace('.stat','') 
                         for p in obj.patterns.all() )
admin.site.register(Recognition,RecognitionAdmin)
admin.site.add_action(analyze)
admin.site.add_action(view_analyzes)
admin.site.disable_action('analyze')
admin.site.disable_action('view_analyzes')

