from django.db import models
from django.http import HttpResponse
from django.core.files import File
from os import system,path
import numpy as np
import matplotlib 
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg
from io import  BytesIO
import csv
import re


class Research(models.Model):
    name = models.CharField('name',max_length=256,
                                help_text='name of research group')
    description = models.TextField('description', blank=True, null=True)
    class Meta:
        db_table = 'research'
    def __str__(self):
        return u'%s' % (self.name)


statistic_strategy_choices = (('1','median'),)

def get_strategy_name(strategy):
        return [x[1] for x in statistic_strategy_choices if x[0]==strategy][0]

class Statistic(models.Model):
    research_name = models.ForeignKey(Research,on_delete=models.SET_NULL,
                                     verbose_name='statistic research name',
                                     null=True,blank=True)
    features = models.FilePathField(
        'input file',
        path=path.join(path.dirname(path.dirname(path.abspath(__file__))),
                                    'media'),
        match='csv',
        max_length=1024,
        help_text='Input csv-file with first features.'
    )
    statistic = models.CharField('output file', max_length=1024,
                                         blank=True,null=True,
                                         help_text='This field is filled ' 
                                                   'automatically.')
    strategy = models.CharField('strategy',
                                choices=statistic_strategy_choices,
                                max_length=1)
    window = models.PositiveSmallIntegerField('window',default=3)
    history = models.TextField('history', blank=True, null=True)
    class Meta:
        db_table = 'statistic'
    def __str__(self):
        return u'st{}-{}{}'.format(
                      self.features.rsplit('/')[-1].replace('.csv',''),
                      [st[1] for st in statistic_strategy_choices 
                             if st[0]==self.strategy][0],
                      self.window
        )


class Recognition(models.Model):
    unknown = models.FilePathField(
        'unknoun csv file', 
        path=path.join(path.dirname(path.dirname(path.abspath(__file__))),
                                                 'media'),
        match='csv',
        max_length=1024
    )
    background = models.ForeignKey(Statistic,on_delete=models.CASCADE,
                                   verbose_name='background',
                                   related_name='background')
    patterns = models.ManyToManyField(Statistic,
                                      verbose_name='patterns',
                                      related_name='patterns')
    strategy = models.CharField('strategy',
                                choices=statistic_strategy_choices, 
                                max_length=1)
    window = models.PositiveSmallIntegerField('window',default=3)
    analyzes = models.CharField('output file', max_length=1024,
                                         blank=True,null=True,
                                         help_text='This field is filled ' 
                                                   'automatically.')
    history = models.TextField('history', blank=True, null=True)
    class Meta:
        db_table = 'recognition'
    def __str__(self):
        return u'%s' % (self.unknown)

def make_statistics(modeladmin,request,queryset):
    ids = queryset.values_list('id')
    stats_list = Statistic.objects.filter(id__in=ids)
    for stat in stats_list:
        strategy_name = get_strategy_name(stat.strategy)
        postfix = '-{}{}.stat'.format(strategy_name,stat.window)
        outname = stat.features.replace('.csv',postfix)
        fcmd = 'make_statistic {} -t {} -s {} -w {}'
        cmd = fcmd.format(stat.features,outname,strategy_name,stat.window)
        system(cmd)
        Statistic.objects.filter(pk=stat.id).update(history=cmd,
                                                    statistic=outname)
make_statistics.short_description = 'Make statistics'

def view_statistics(modeladmin,request,queryset):
    plt.style.use('seaborn-notebook')
    s=''
    ids = queryset.values_list('id')
    M = len(ids)
    N=1
    for stat in Statistic.objects.filter(id__in=ids):
        f = open(stat.statistic)
        head_line = f.readline()
        f.close()
        C = head_line.count(',')+1
        if C>N:
            N = C
    fig, axes = plt.subplots(nrows=M,ncols=N+1,figsize=((N+1)*4,M*4))
    axes.resize(M,N+1)
    i=0
    for stat in Statistic.objects.filter(id__in=ids):
        statistic_data = np.loadtxt(stat.statistic,dtype='int')
        f = open(stat.statistic)
        head_line = f.readline()
        f.close()
        head_list = head_line[1:].strip().split(',')
        N = len(head_list)
        for j in range(N):
            hist = statistic_data[2:,j]
            mn,mx = statistic_data[:2,j]
            k = np.linspace(start=mn,stop=mx,num=hist.shape[0]+1,
                            endpoint=True)
            if True:
                axes[i,j].set_title(head_list[j],fontweight="bold",size=14)
                sh = np.sum(hist)
                axes[i,j].bar(
                    left=0.5*(k[:hist.shape[0]]+k[1:]),
                    height=hist/sh,
                    width=.6*(k[1]-k[0]),
                    align='center'
                )
                axes[i,j].grid(True)
            msg = stat.statistic.split('/')[-1]
            msg += '\n\n'
            msg += [x[1] for x in statistic_strategy_choices if x[0]==stat.strategy][0]
            msg += '\n\n'
            msg += 'window={}'.format(stat.window)
            axes[i,N].text(0.1,0.6, msg,fontsize=12)
            axes[i,N].axis('Off')
        i += 1
    canvas = FigureCanvasAgg(fig)
    buf = BytesIO()
    plt.savefig(buf,format='png')
    plt.close(fig)
    response = HttpResponse(buf.getvalue(),content_type='image/png')
    return response

def analyze(modeladmin,request,queryset):
    ids = queryset.values_list('id')
    researches_list = Recognition.objects.filter(id__in=ids)
    for research in researches_list:
        strategy_name = get_strategy_name(research.strategy)
        patterns=' '.join([ str(st.statistic) 
            for st in Statistic.objects.filter(patterns__id=research.id)])
        out_file_name = research.unknown.replace('.csv','-analyzed.csv')
        fcmd = 'analyze_statistic {} -n {} -p {} -s {} -w {} -o {}'
        cmd = fcmd.format(research.unknown,research.background.statistic,
                          patterns,strategy_name,research.window,
                          out_file_name)
        system(cmd)
        Recognition.objects.filter(pk=research.id).update(history=cmd,
                                                          analyzes=out_file_name)
analyze.short_description = 'Analyze'


def view_analyzes(modeladmin,request,queryset):
    ids = queryset.values_list('id')
    researches_list = Recognition.objects.filter(id__in=ids)
    msg=''
    for research in researches_list:
        msg += '<h3>{}</h3>'.format(research.analyzes)
        with open(research.analyzes,newline='\n') as data_file:
            rows_list = csv.reader(data_file,delimiter=',')
            msg += '<table border=1>'
            is_title = True
            for row in rows_list:
                if is_title:
                    msg += '<tr><th>'
                    msg += '</th><th>'.join(row)
                    msg += '</th</tr>'
                    is_title = False
                    continue
                if research.background.statistic==row[-1]:
                    color='#00ff80'
                else:
                    str_id = row[-1]
                    nn = re.search('p[0-9]{2}',str_id)
                    color='#ff{}00'.format(3*int(str_id[nn.start()+1:nn.end()]))
                msg += '<tr bgcolor="{}"><td>'.format(color)
                row[-1] = row[-1].split('/')[-1].replace('.stat','')
                msg += '</td><td>'.join(row)
                msg += '</td></tr>'
            msg += '</table>'
            #msg += ' '.join(data_row)
    response = HttpResponse(msg,content_type='text/html; charset=utf-8')
    return response
view_analyzes.short_description = 'View analyzes'

