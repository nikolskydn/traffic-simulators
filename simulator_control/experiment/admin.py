from django.contrib import admin
from experiment.models import *

# Register your models here.

class CommandAdmin(admin.ModelAdmin):
        list_display = ('name','dst_iface','simulator','node','src_iface','dst_port','src_port')
        actions = ('commands_preview',)
admin.site.register(Command,CommandAdmin)
admin.site.add_action(commands_preview)
admin.site.disable_action('commands_preview')

class DumperAdmin(admin.ModelAdmin):
    list_display = ('name', 'node', 'command')
admin.site.register(Dumper,DumperAdmin)

class ExperimentAdmin(admin.ModelAdmin):
        list_display = ('name','duration','start','get_commands','get_dumpers','get_history')
        actions = ('run_experiments','run_analyse')
        def get_commands(self, obj):
             return "\n".join([c.name for c in obj.commands.all()])
        def get_dumpers(self, obj):
             return "\n".join([d.name for d in obj.dumpers.all()])
        def get_history(self, obj):
            return obj.history[:100]+'...'
admin.site.register(Experiment,ExperimentAdmin)
admin.site.add_action(run_experiments)
admin.site.add_action(run_analyse)
admin.site.disable_action('run_experiments')
admin.site.disable_action('run_analyse')

class DumpAdmin(admin.ModelAdmin):
    list_display = ('dumper','experiment','remote_name')
    actions = ('copy_remote_file',)
admin.site.register(Dump,DumpAdmin)
admin.site.add_action(copy_remote_file)
admin.site.disable_action('copy_remote_file')

class TaskAdmin(admin.ModelAdmin):
    list_display = ('command','experiment','number')
admin.site.register(Task,TaskAdmin)

#class DumpAdmin(admin.ModelAdmin):
#    list_display = ('dumper','experiment','outfile')
#admin.site.register(Dump,DumpAdmin)


