from django.db import models
from django.http import HttpResponse
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.models import Q
from simulator.models import *
from iface.models import *
import os
import datetime

class Command(models.Model):
    name = models.CharField('command name', max_length=256,unique=True)
    dst_iface = models.ForeignKey(Iface,on_delete=models.CASCADE,verbose_name='dest. inet. addr.',related_name='dst_inet_addr')
    simulator = models.ForeignKey(Simulator,on_delete=models.CASCADE,verbose_name='simulator')
    node = models.ForeignKey(Node,on_delete=models.CASCADE,verbose_name='node')
    src_iface = models.ForeignKey(Iface,on_delete=models.SET_NULL,verbose_name='sour. inet. addr.',related_name='src_inet_addr',null=True,blank=True)
    dst_port = models.PositiveIntegerField(null=True,blank=True, validators=[MinValueValidator(0), MaxValueValidator(65535)])
    src_port = models.PositiveIntegerField(null=True,blank=True, validators=[MinValueValidator(0), MaxValueValidator(65535)])
    class Meta:
        db_table = 'command'
        ordering = ('name',)
    def __str__(self):
        return u'%s' % (self.name)

class Dumper(models.Model):
    name=models.CharField('dumper name', max_length=256, unique=True)
    command=models.CharField('command', max_length=1024)
    node = models.ForeignKey(Node,on_delete=models.CASCADE,verbose_name='node')
    directory = models.CharField('directory', max_length=256, default='/tmp/')
    describtion=models.TextField('describtion', blank=True, null=True)
    class Meta:
        db_table = 'dumper'
    def __str__(self):
        return u'%s' % (self.name)

class Experiment(models.Model):
    name = models.CharField('experiment name', max_length=256, unique=True)
    duration = models.PositiveIntegerField('duration, sec.', default=60)
    commands = models.ManyToManyField(Command, through='Task')
    dumpers = models.ManyToManyField(Dumper,through='Dump')
    start = models.CharField('start time', max_length=128, default='-')
    history = models.TextField('history', blank=True)
    class Meta:
        db_table = 'experiment'
        ordering = ('name',)
    def __str__(self):
        return u'%s' % (self.name)

class Task(models.Model):
    command = models.ForeignKey(Command, related_name='command', on_delete=models.CASCADE)
    experiment = models.ForeignKey(Experiment, related_name='experiment', on_delete=models.CASCADE)
    number = models.PositiveSmallIntegerField('Number of processes', default=1,validators=[MinValueValidator(1), MaxValueValidator(128)])
    class Meta:
        db_table = 'task'
    def __str__(self):
        return u'%s.%s=%s' % (self.command.name, self.experiment.name, self.number)

class Dump(models.Model):
    dumper = models.ForeignKey(Dumper, related_name='dump_with_dumper', on_delete=models.CASCADE)
    experiment = models.ForeignKey(Experiment, related_name='dump_with_experiment', on_delete=models.CASCADE)
    remote_name = models.CharField('remote_name', max_length=256, default='-',help_text='auto')
    class Meta:
        db_table = 'dump'
    def __str__(self):
        return u'%s.%s=%s' % (self.dumper.name, self.experiment.name, self.remote_name)

def make_base_command(c,duration=5,number=1):
        return "ssh {}@{} -i {} 'screen -dmS {} sudo {} {} --time {}'\n".format(
                c.node.account.login,
                Iface.objects.get(Q(node=c.node)&Q(iface_type='c')).inet_addr,
                c.node.account.ssh_key,
                c.name+'-'+str(number),
                c.simulator,
                c.dst_iface.inet_addr,
                duration
        )

class DumperValues:
    def __init__(self,dumper):
        self.login=dumper.node.account.login
        self.iface=Iface.objects.get(Q(node=dumper.node)&Q(iface_type='c')).inet_addr
        self.ssh_key=dumper.node.account.ssh_key
        self.name=dumper.name
        self.command=dumper.command
        self.directory=dumper.directory


def make_dumper_command(dumper_values, duration=5, outfile='tmp.cap'):
        return "ssh {}@{} -i {} 'screen -dmS {} {}'\n".format(
                dumper_values.login,
                dumper_values.iface,
                dumper_values.ssh_key,
                dumper_values.name,
                dumper_values.command,
        ).replace('TIME',str(duration)).replace('OUTFILE',outfile)

def commands_preview(modeladmin, request, queryset):
    ids = queryset.values_list('id')
    cmds_list=Command.objects.filter(id__in=ids)
    s=''
    for c in cmds_list:
        s+=make_base_command(c)
    response = HttpResponse(s,content_type="text/plain")
    return response
commands_preview.short_description = 'Commands preview'

def copy_remote_file(modeladmin, request, queryset):
    ids = queryset.values_list('id')
    for i in ids:
        d=Dump.objects.get(pk=i[0])
        dm_values=DumperValues(d.dumper)
        media_root=os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'media/')
        cmd='scp -i '+dm_values.ssh_key+' '+d.remote_name+' '+media_root
        print(cmd)
        os.system(cmd)
copy_remote_file.short_description = 'Copy'

def run_experiments(modeladmin, request, queryset):
    ids = queryset.values_list('id')
    for i in ids:
        now = datetime.datetime.now()
        current_time='{}-{}-{} {}:{}'.format(now.year,now.month,now.day,now.hour,now.minute)
        Experiment.objects.filter(pk=i[0]).update(
            start=current_time
        )
        s=''
        dump_files_list=''
        dumpers_list=Dumper.objects.filter(experiment=i[0])
        for d in dumpers_list:
            dm_values=DumperValues(d)
            print(dm_values)
            curr_outfile=dm_values.directory
            curr_outfile+=Experiment.objects.get(id=i[0]).name
            curr_outfile+='_'+current_time.translate(str.maketrans(" :","__"))
            if d.command.startswith('dumpcap'):
                curr_outfile+='.cap'
            elif d.command.startswith('tshark-netflow'):
                curr_outfile+='.csv'
            else:
                curr_outfile+='dump'
            dump_files_list+=dm_values.login+'@'+dm_values.iface+':'+curr_outfile+' '
            Dump.objects.filter(pk=d.id).update(remote_name=dm_values.login+'@'+dm_values.iface+':'+curr_outfile+' ')
            cmd=make_dumper_command(
                dm_values,
                duration=Experiment.objects.get(id=i[0]).duration+30,
                outfile=curr_outfile
            )
            os.system(cmd)
            s+=cmd+'\n'
        tasks_list=Task.objects.filter(experiment=i[0])
        for t in tasks_list:
            for j in range(t.number):
                cmd=make_base_command(
                        t.command,
                        duration=Experiment.objects.get(id=i[0]).duration,
                        number=j
                )
                os.system(cmd)
                s+=cmd+'\n'
        now = datetime.datetime.now()
        s+='dump files list = [ {} ]\n'.format(dump_files_list)
        Experiment.objects.filter(pk=i[0]).update(history=s)
run_experiments.short_description = 'Run experiment'

# for new application

def run_analyse(modeladmin,request,queryset):
    from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
    from matplotlib.figure import Figure
    s=''
    ids = queryset.values_list('id')
    for e in Experiment.objects.filter(id__in=ids):
        s+=e.name
    fig=Figure()
    ax=fig.add_subplot(111)
    x=[1,2,3,4]
    y=[1,4,9,16]
    ax.bar(x, y)
    canvas=FigureCanvas(fig)
    response=HttpResponse(content_type='image/png')
    canvas.print_png(response)
    return response
