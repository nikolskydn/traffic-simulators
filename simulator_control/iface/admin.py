from django.contrib import admin

from iface.models import *

class AccountAdmin(admin.ModelAdmin):
    list_display = ('name', 'login', 'password', 'ssh_key')
admin.site.register(Account,AccountAdmin)

class NodeAdmin(admin.ModelAdmin):
    list_display = ('hostname', 'account', 'description')
admin.site.register(Node,NodeAdmin)

class IfaceAdmin(admin.ModelAdmin):
    list_display = ('node', 'name', 'inet_addr', 'iface_type')
    ordering = ('node',)
    raw_id_fields = ('node',)
    search_fields = ('name', 'node__hostname')
    list_filter = ('node', 'iface_type')
    actions =('check_ifaces',)

admin.site.register(Iface,IfaceAdmin)

admin.site.add_action(check_ifaces)
admin.site.disable_action('check_ifaces')
