from django.db import models
from django.http import HttpResponse

class Account(models.Model):
    name = models.CharField('name', max_length=256, default='1',unique=False)
    login = models.CharField('login', max_length=256)
    password = models.CharField('password', max_length=256)
    ssh_key = models.CharField('ssk key', max_length=256,help_text='Path to ssh key.')
    class Meta:
        db_table = 'account'
    def __str__(self):
        return u'%s' % (self.name)

class Node(models.Model):
    hostname = models.CharField('hostname',unique=True, max_length=256, help_text=u'Node name.')
    account = models.ForeignKey(Account,on_delete=models.SET_NULL,verbose_name='account for node',null=True,blank=True)
    description = models.CharField(u'describtion for node', max_length=256, blank=True, help_text=u'About node.')
    class Meta:
        db_table = 'node'
    def __str__(self):
        return u'%s' % (self.hostname)

class Iface(models.Model):
    name = models.CharField('iface name',max_length=32)
    inet_addr = models.GenericIPAddressField('inet address',blank=True,null=True)
    node = models.ForeignKey(Node,on_delete=models.CASCADE,verbose_name='node for iface')
    iface_type_choices = ( ('w','Work'),('c','Control') )
    iface_type = models.CharField('iface type',choices=iface_type_choices,max_length=1,default='w',help_text=u'Simulators work with working interfaces. So, traffic flows for research are moved between the working interfaces.')
    class Meta:
        db_table = 'iface'
    def __str__(self):
        return u'%s on %s' % (self.inet_addr,self.node.hostname)

def check_ifaces(modeladmin, request, queryset):
    import paramiko
    ids = queryset.values_list('id')
    s=''
    ifases_list=Iface.objects.filter(id__in=ids)
    for iface in ifases_list:
        if iface.iface_type=='c':
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(iface.inet_addr, username=iface.node.account.login, password=iface.node.account.password)
            cmd="sudo ifconfig {} | head -2".format(iface.name)
            stdin, stdout, stderr = ssh.exec_command(cmd)
            s+=iface.inet_addr+' '+iface.node.account.login+'\n'
            s+='[\n'+' '.join(stdout.readlines())+']\n\n'
        if iface.iface_type=='w':
            s+=iface.inet_addr+' is  work iface\n\n'
    response = HttpResponse(s,content_type="text/plain")
    return response
check_ifaces.short_description = 'Check ifaces'
