from django.contrib import admin
from simulator.models import *

class SimulatorAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'simulator_type')
admin.site.register(Simulator,SimulatorAdmin)

class ParameterAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
admin.site.register(Parameter,ParameterAdmin)

class SettingAdmin(admin.ModelAdmin):
    list_display = ('simulator', 'parameter', 'value')
admin.site.register(Setting,SettingAdmin)

