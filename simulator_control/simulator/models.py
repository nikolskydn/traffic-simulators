from django.db import models
from django.http import HttpResponse

class Parameter(models.Model):
    name = models.CharField('parameter name', unique=True, max_length=256)
    description = models.CharField('parameter description', max_length=1024, blank=True, null=True)
    class Meta:
        db_table = 'parameter'
    def __str__(self):
        return u'%s' % (self.name)

class Simulator(models.Model):
    name = models.CharField('simulator name', unique=True, max_length=256)
    description = models.CharField('simulator description', max_length=1024, null=True, blank=True)
    SIM_TYPE_CHOICES = (('a','Abnormal'), ('n','Normal'), ('u','Utilites'),)
    simulator_type = models.CharField(max_length=1,choices=SIM_TYPE_CHOICES,default=u'a')
    setting = models.ManyToManyField(Parameter, through='Setting')
    class Meta:
        db_table = 'simulator'
    def __str__(self):
        return u'%s' % (self.name)

class Setting(models.Model):
    simulator = models.ForeignKey(Simulator, related_name='simulator', on_delete=models.CASCADE)
    parameter = models.ForeignKey(Parameter, related_name='parameter', on_delete=models.CASCADE)
    value = models.CharField('value',max_length=256)
    class Meta:
        db_table = 'setting'
    def __str__(self):
        return u'%s.%s=%s' % (self.simulator.name, self.parameter.name,self.value)

