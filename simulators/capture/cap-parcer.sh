#!/bin/bash
declare -i o1 o2 o3 o4
echo "SrcAddr,DstAddr,SrcPort,DstPort,Protocol,TCPFlags,Octets,Packets,StringData"
tshark -r $1 -d udp.port==9989,cflow -V | while read line; do
    isFlowLine=`echo "${line}" |  grep -E 'Flow [0-9]+|pdu [0-9]+/[0-9]+'`
    StringData=$isFlowLine
    if [ -n "${isFlowLine}" ]; then
        for (( l=0; l<21; l++ )); do
            read record
            if [ $l -le 0 ]; then 
                StringData="     $StringData-$record"
            fi
            if [ -n "`echo $record | grep SrcAddr:`" ]; then
                SrcAddrStr=$(echo -n ${record} | sed 's/SrcAddr: \([0-9\.]*\).*/\1/')
                #IFS=. read o1 o2 o3 o4 <<< "${SrcAddrStr}"
                #SrcAddr=$((o1*256**3+o2*256**2+o3*256+o4))
                SrcAddr=$SrcAddrStr
            fi
            if [ -n "`echo $record | grep DstAddr:`" ]; then
                DstAddrStr=$(echo -n ${record} | sed 's/DstAddr: \([0-9\.]*\).*/\1/')
                #IFS=. read o1 o2 o3 o4 <<< "${DstAddrStr}"
                #DstAddr=$((o1*256**3+o2*256**2+o3*256+o4))
                DstAddr=$DstAddrStr
            fi
            if [ -n "`echo $record | grep SrcPort:`" ]; then
                SrcPort=$(echo -n ${record} | sed 's/SrcPort: \([0-9]*\).*/\1/')
            fi
            if [ -n "`echo $record | grep DstPort:`" ]; then
                DstPort=$(echo -n ${record} | sed 's/DstPort: \([0-9]*\).*/\1/')
            fi

            if [ -n "`echo $record | grep Protocol:`" ]; then
                Protocol=$(echo -n ${record} | sed 's/Protocol: \([0-9]*\).*/\1/')
            fi

            if [ -n "`echo $record | grep 'TCP Flags':`" ]; then
                Hex=$(echo -n ${record} | sed 's/TCP Flags: \(0x[0-9A-Fa-a]\{1,\}\)/\1/')
                TCPFlags=$((Hex))
            fi

            if [ -n "`echo $record | grep 'Octets':`" ]; then
                Octets=$(echo -n ${record} | sed 's/Octets: \([0-9]\{1,\}\)/\1/')
            fi

            if [ -n "`echo $record | grep Packets:`" ]; then
                Packets=$(echo -n ${record} | sed 's/Packets: \([0-9]*\).*/\1/')
            fi
        done
        echo "$SrcAddr,$DstAddr,$SrcPort,$DstPort,$Protocol,$TCPFlags,$Octets,$Packets,$StringData"
    fi
done
