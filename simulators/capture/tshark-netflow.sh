#!/bin/bash
declare -i o1 o2 o3 o4
USAGE="Usage: `basename $0` [-h] -o out_file -i iface -t time ports_list\nports_list := port1 port 2 ..."
while getopts  ho:i:t: OPT; do
    case "$OPT" in
        h)
            echo -e $USAGE
            exit 0
        ;;
        o)
            OUTFILE=$OPTARG
        ;;
        i)
            IFACE=$OPTARG
        ;;
        t)
            TIME=$OPTARG
        ;;
   esac
done
shift `expr $OPTIND - 1`
PORTS=( "$@" )
dkeys=$(for p in ${PORTS[*]} ; do echo -n "-d udp.port==${p},cflow "; done)
ports=$(for i in ${!PORTS[*]}; do echo -n "port ${PORTS[$i]} "; if [ $[i+1] -lt ${#PORTS[*]} ]; then echo -n "or "; else echo -n ' '; fi; done)
echo "SrcAddr,DstAddr,SrcPort,DstPort,Protocol,TCPFlags,Octets,Packets" > "${OUTFILE}" 
timeout ${TIME} tshark -i ${IFACE} ${dkeys} -V  ${ports} |  grep -B1 -A20 "SrcAddr"  | while read line; do
    isFlowLine=`echo "${line}" |  grep -E 'Flow [0-9]+|pdu [0-9]+/[0-9]+'`
    if [ -n "${isFlowLine}" ]; then
        for (( l=0; l<21; l++ )); do
            read record
            if [ -n "`echo $record | grep SrcAddr:`" ]; then
                SrcAddrStr=$(echo -n ${record} | sed 's/SrcAddr: \([0-9\.]*\).*/\1/')
                IFS=. read o1 o2 o3 o4 <<< "${SrcAddrStr}"
                SrcAddr=$((o1*256**3+o2*256**2+o3*256+o4))
            fi
            if [ -n "`echo $record | grep DstAddr:`" ]; then
                DstAddrStr=$(echo -n ${record} | sed 's/DstAddr: \([0-9\.]*\).*/\1/')
                IFS=. read o1 o2 o3 o4 <<< "${DstAddrStr}"
                DstAddr=$((o1*256**3+o2*256**2+o3*256+o4))
            fi
            if [ -n "`echo $record | grep SrcPort:`" ]; then
                SrcPort=$(echo -n ${record} | sed 's/SrcPort: \([0-9]*\).*/\1/')
            fi
            if [ -n "`echo $record | grep DstPort:`" ]; then
                DstPort=$(echo -n ${record} | sed 's/DstPort: \([0-9]*\).*/\1/')
            fi

            if [ -n "`echo $record | grep Protocol:`" ]; then
                Protocol=$(echo -n ${record} | sed 's/Protocol: \([0-9]*\).*/\1/')
            fi

            if [ -n "`echo $record | grep 'TCP Flags':`" ]; then
                Hex=$(echo -n ${record} | sed 's/TCP Flags: \(0x[0-9A-Fa-a]\{1,\}\)/\1/')
                TCPFlags=$((Hex))
            fi

            if [ -n "`echo $record | grep 'Octets':`" ]; then
                Octets=$(echo -n ${record} | sed 's/Octets: \([0-9]\{1,\}\)/\1/')
            fi

            if [ -n "`echo $record | grep Packets:`" ]; then
                Packets=$(echo -n ${record} | sed 's/Packets: \([0-9]*\).*/\1/')
            fi
        done
        echo "$SrcAddr,$DstAddr,$SrcPort,$DstPort,$Protocol,$TCPFlags,$Octets,$Packets"
    fi
done >> "${OUTFILE}" 
