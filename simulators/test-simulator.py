#!/usr/bin/python3
# coding: utf-8 
import paramiko
import csv
import sys
import os
acc={}
HOST=sys.argv[1]
with open("accounts.csv") as facc:
    reader = csv.DictReader(facc, delimiter=';')
    for line in reader:
        if line['host']==HOST:
           acc.update( { 'ip':line['ip'], 'login':line['login'], 'pass':line['pass'], 'vwip':line['vwip'], 'vwiface':line['vwiface'], 'vcip':line['vcip'], 'vclogin':line['vclogin'], 'vckey':line['vckey']} )
           break
print(acc)
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(acc['ip'], username=acc['login'], password=acc['pass'])
gen_flood_cmd="screen -dmS gentcp sudo gentcpflood {}  --time 60".format(acc['vwip'])
print(gen_flood_cmd)
stdin, stdout, stderr = ssh.exec_command(gen_flood_cmd)
#print(stdout.readlines()
tshark_cmd="ssh {}@{} -i {} 'sudo tshark -i {}'".format(acc['vclogin'],acc['vcip'],acc['vckey'],acc['vwiface'])
print(tshark_cmd)
os.system(tshark_cmd)
