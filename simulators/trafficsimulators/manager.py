#!/usr/bin/python3

# The module was created exclusively for research purposes.

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), os.path.pardir))

from trafficsimulators.simulator import Simulator
from trafficsimulators.tcpflood import TCPFlood
from trafficsimulators.tcpconnectionflood import TCPConnectionFlood
from trafficsimulators.udpfragmentflood import UDPFragmentFlood
from scapy.all import *

class SimulatorFactory:
    def createSimulator(self):
        pass

class TCPFloodFactory(SimulatorFactory):
    def createSimulator(self):
        return TCPFlood()

class TCPConnectionFloodFactory(SimulatorFactory):
    def createSimulator(self):
        return TCPConnectionFlood()

class UDPFragmentFloodFactory(SimulatorFactory):
    def createSimulator(self):
        return UDPFragmentFlood()

class SimulatorManager:

    def __init__(self,factory):
        self.factory=factory
        self.simulator=factory.createSimulator()

    def set_params(self,**kwargs):
        self.simulator.set_params(**kwargs)

    def play(self,duration):
        if self.simulator.is_debug():
            print(self.simulator)
        self.simulator.start()
        self.simulator.join(duration)
        if self.simulator.is_alive():
            self.simulator.terminate()
            self.simulator.join()

if __name__=='__main__':
    s1=SimulatorManager(TCPFloodFactory())
    s1.set_params( dst_inet_addr='192.169.0.150', tcp_flags='S', debug=False)
    s1.play(1)

    s2=SimulatorManager(TCPConnectionFloodFactory())
    s2.set_params( dst_inet_addr='192.169.0.150', port_dst=8080, debug=False)
    s2.play(1)

    s3=SimulatorManager(UDPFragmentFloodFactory())
    s3.set_params( dst_inet_addr='192.169.0.150', debug=False)
    s3.play(1)

