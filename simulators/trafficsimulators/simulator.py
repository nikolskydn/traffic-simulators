#!/usr/bin/python3

# The module was created exclusively for research purposes.

from multiprocessing import Process
from abc import ABCMeta, abstractmethod

class Simulator(Process):

    __metaclass__=ABCMeta

    def __init__(self):
        self.kwargs={}
        super().__init__()

    def is_debug(self):
        if 'debug' in self.kwargs:
            return self.kwargs['debug']
        else: return False


    def set_params(self,**kwargs):
        self.kwargs.update(kwargs)

    def __str__(self):
        info=[]
        for key,value in self.kwargs.items():
            info+=["\033[0;32;40m{0}=\033[0;33;40m{1}\033[0m".format(key,value)]
        return '\n'.join(info)


if __name__=='__main__':
    sim=Simulator()
    sim.set_params(x=1,y=2)
    print(sim)

