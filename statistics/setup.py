#!/usr/bin/python3
from setuptools import setup

setup(name='traffic_statistic', version='0.0.0', 
      packages=['traffic_statistic'],
      description='Module for analyzing network traffic and other data.',
      author='Nikolskii D. N.', author_email='nikolskydn@mail.ru',
      entry_points={'console_scripts': 
          ['analyze_statistic=traffic_statistic.analyze_statistic:main',
          'draw_statistic=traffic_statistic.draw_statistic:main',
          'make_statistic=traffic_statistic.make_statistic:main']},
)

