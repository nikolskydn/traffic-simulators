#!/usr/bin/python3
import numpy as np
import argparse
import logging
import scipy.sparse as sp
from traffic_statistic import historgam as ht
from traffic_statistic import transform as tf
import csv
import re

def get_params():
    parser = argparse.ArgumentParser(description='Analize unknow statistic.')
    parser.add_argument('-n', '--normal', help='Normal statistic file', 
                        type=open)
    parser.add_argument('unknow', help='Unknow csv first features file', 
                        type=open)
    parser.add_argument('-p', '--patterns', nargs='+',
                        help='Pattern statistic files', type=open)
    parser.add_argument('-s', '--strategy', help="Transform strategy", 
                        choices = ['median'], 
                        type=str, default='median')
    parser.add_argument('-w', '--window', 
                        help='Window for Feature.',
                        type=int, default=3)
    parser.add_argument('-o', '--outfile',help='Outfile with analyzes', 
                        type=str)
    return  parser.parse_args()




def main():

#    logging.basicConfig(format='%(filename)s:%(lineno)d %(levelname)s' 
#                        ' %(asctime)s %(message)s', level=logging.DEBUG,
#                        filename='analize_statistic.log')
    logging.basicConfig(format='<h4>%(filename)s [%(lineno)d] %(levelname)s</h4>' 
                        '<p> %(message)s</p>', 
                        #level=logging.DEBUG,
                        level=logging.ERROR,
                        filename='analize_statistic.html')
    logger = logging.getLogger(__name__)
    logger.info('<h1>analize_statistic</h1>')

    np.set_printoptions(precision=3, suppress=True, linewidth=150,
                        formatter={'float':lambda x: '%6.1f'%x})
    args = get_params()
    normal = np.loadtxt(args.normal,comments='#',dtype=np.dtype(int),
                        skiprows=1)
    patterns_list = [ np.loadtxt(pattern.name,comments='#', 
                                dtype=np.dtype(int),skiprows=1)
                     for pattern in args.patterns ]
    unknow = np.loadtxt(args.unknow,delimiter=',',comments='#',
                        dtype=np.dtype(int),skiprows=1)



    first_features = tf.Features(window=args.window)
    first_features.data = unknow

    if args.strategy=='median':
        if not args.window&1:
            logger.error('Set the odd window for DH-transform. '
                        'Your choice of '
                        'window={} is not correct.'.format(args.window))
            return
        curr_strategy = tf.Median()
    else:
        logger.error('Strategy not found')
        return
    logger.debug(curr_strategy)

#    alpha=0.05
#    beta=0.05
    alpha=0.01
    beta=0.01
    A=(1-beta)/alpha
    B=beta/(1-alpha)
    logger.debug('A={} B={}'.format(A,B))
    second_features = np.array([curr_strategy(elem) 
                                for elem in first_features])

    anomalies = sp.lil_matrix((len(patterns_list),unknow.shape[0]))
    for pidx,pattern in enumerate(patterns_list):
        lh_functor = ht.LikelihoodRatio(normal_array=normal,
                                        pattern_array=pattern,
                                        logger=logger)
        lhs_buffer = []
        curr_anomalies = sp.lil_matrix((len(patterns_list),unknow.shape[0]))
        for idx,sfrow in enumerate(second_features):
            logger.debug('analize idx={} sfrow={}'.format(idx,sfrow))
            #print('{} [!] Likelihood({})'.format(idx,sfrow),end=' ') #################################
            lhr = lh_functor(sfrow)
            #print(' =' , lhr) ########################################################################
            logger.debug(' lh = {} '.format(lhr))
            if lhr > 1: 
                lhs_buffer.append(lhr)
                logger.debug('lilelihood buffers={}'.format(lhs_buffer))
            Lh = np.prod(np.array(lhs_buffer))
            if Lh >= A:
                logger.info('Alarm: Lh = {:6.1f} in block from {:4d} ' 
                            'to {:4d} '.format(Lh,idx,
                                               idx+first_features.window))
                curr_anomalies[pidx,idx:idx+first_features.window] = Lh
                mask = curr_anomalies>anomalies
                anomalies[mask] = curr_anomalies[mask]
                lhs_buffer.clear()
        logger.debug(anomalies.toarray())
    #если требуется сообщить об одной аномалии с максимальным Lh
    #result = np.argmax(np.r_[np.zeros((1,anomalies.shape[1])),anomalies.A],
    #                   axis=0)
    # если требуется сообщить обо всех аномалиях
    result = anomalies.A
    #
    full_patterns = args.patterns
    #full_patterns.insert(0,args.normal)
    with open(args.unknow.name,'r',newline='\n') as ufile:
        out_header = ufile.readline().rstrip().split(',')
        print(out_header)
    out_header.append('Pattern')
    if args.outfile is None:
        out_file_name = args.unknow.name.replace('.csv','-analyzed.csv')
    else:
        out_file_name = args.outfile
    with open(out_file_name,'w',newline='\n') as outfile:
        writer = csv.writer(outfile, delimiter=',')
        writer.writerow(out_header)
        for i in range(unknow.shape[0]):
            data_row = [ d for d in unknow[i] ]
            #если требуется сообщить об одной аномалии с максимальным Lh
            #data_row.append(str(full_patterns[result[i]].name))
            #если требуется сообщить обо всех аномалиях
            info = ''
            sumlh = np.sum(result[:,i]) + 1e-15
            for j in range(result.shape[0]):
                prob = result[j][i]/sumlh
                if prob>1e-6:
                    pname = re.search(r'p\d+',full_patterns[j].name)
                    if pname == '': 
                        pname = full_patterns[j].name
                    info += '#{}:{}'.format(pname.group(),prob)
            if info == '':
                data_row.append('normal')
            else:
                data_row.append(info)
            #
            writer.writerow(data_row)


if __name__=="__main__":
    main()
