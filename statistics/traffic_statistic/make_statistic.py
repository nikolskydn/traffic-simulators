#!/usr/bin/python3
import numpy as np
import argparse
import logging
from traffic_statistic import historgam as ht
from traffic_statistic import transform as tf

def get_params():
    parser = argparse.ArgumentParser(description="Make netflow data statistics")
    parser.add_argument('from_csv', help="Input csv file", type=open)
    parser.add_argument('-t', '--to_stat', 
                        help="Output statstic file", type=str)
    parser.add_argument('-s', '--strategy', help="Transform strategy",
                        choices = ['median'], 
                        type=str, default='median')
    parser.add_argument('-w', '--window', 
                        help="Window for Feature. Must be odd integer.",
                        type=int, default=11)
    return parser.parse_args()

def print_error(msg):
    print('\033[31;1mError\033[2m: {}\033[0m'.format(msg))

def main():

    logging.basicConfig(format='<h4>%(filename)s [%(lineno)d] %(levelname)s</h4>' 
                        '<p> %(message)s</p>', 
                        level=logging.DEBUG,
                        filename='make_statistic.html')
    logger = logging.getLogger(__name__)
    logger.info('<h1>make_statistic</h1>')


    args=get_params()
    ff = tf.Features(window=args.window)
    cdata = np.loadtxt(args.from_csv.name, dtype=np.dtype(int),
                       delimiter=',', skiprows=1)
    curr_strategy = None
    if args.strategy=='median':
        if not args.window&1:
            print_error('Set the odd window. '
                        'Your choice of '
                        'window={} is not correct.'.format(args.window))
            return
        curr_strategy = tf.Median()
        ff.data = cdata
    else:
        print_error('Strategy not found')
        return
        
    smaker = ht.MakeStatistic(curr_strategy)
    stats = smaker(ff)
    if args.to_stat is None:
        stat_name = args.from_csv.name.replace('.csv','.stat')
    else:
        stat_name  = args.to_stat
    np.savetxt(fname=stat_name, fmt='%7i', X=stats,
               header=curr_strategy._secondary_features_head)

if __name__=="__main__":
    main()
