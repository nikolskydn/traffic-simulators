#!/usr/bin/python3
import numpy as np

class Features(object):
    """ Container for storage of features.

    Used to iterate the features in a window.

    """


    def __init__(self, window=7):
        self._data = None
        self._counter = 0
        self._window = window
        self._rows = 0
        self._cols = 0

    def _data_get(self):
        return self._data

    def _data_set(self, array):
        self._rows,self._cols = array.shape
        self._data = array

    data = property(fget=_data_get, fset=_data_set)

    def _window_get(self):
        return self._window

    def _window_set(self, value):
        self._window = value

    window = property(fget=_window_get, fset=_window_set)

    def __iter__(self):
        self._counter = 0
        return self

    def __next__(self):
        idx=self._counter
        if idx < self._rows-self._window+1:
            self._counter += 1
            return self._data[idx:idx+self._window,:]
        else:
            raise StopIteration

class Median(object):
    """ The median value.

    """
   
    def __init__(self):

        self._first_features_head = 'SrcAddr,DstAddr,SrcPort,DstPort,'\
                                    'Protocol,TCPFlags,ICMP,Octets,Packets'
        self._secondary_features_head = 'SrcPort,DstPort,'\
                                        'Protocol,TCPFlags,ICMP,Load'

    def __call__(self,data):
        rows, cols = data.shape
        L = rows // 2 
        #b = data[:-1,:2]!=data[1:,:2]
        #c01 = np.array([np.sum([2**i*b[i,j] for i in range(b.shape[0])]) 
        #                 for j in range(b.shape[1])])
        port_check_list = np.array([0,53,123,1900])
        c26 = np.median(data[:,2:7],axis=0) 
        c23 = np.array([-1,-1])
        for idx,port in enumerate(port_check_list):
           c23[c26[:2]==port] = idx
        load = data[:,7]/data[:,8]
        #hilbert = np.stack(1/i if i%2==1 else 0 for i in range(-L,L+1))
        #lh = np.dot(load,hilbert)
        #c7 = load[L]+lh if lh>0 else -load[L]+lh
        c7 = np.median(load)
        return np.r_[c23, c26[2:], c7]

#        глубина модуляции
#        (mx-mn)/(mx+mn)
#        Delta: число совпадений или несовпадений
#        c01 = np.sum(data[:-1,:2]!=data[1:,:2],axis=0) / rows


if __name__=="__main__":
    np.set_printoptions(precision=2, suppress=True, linewidth=150,
                        formatter={'float':lambda x: '%10.2f'%x})
    ff=Features()
    ff.window=5
    fLA=CausalOperator()
    ff.data = np.array([
        [1,2,3,4,5,6,7,1,8],
        [1,2,4,5,6,7,8,2,1],
        [3,4,4,5,7,8,1,3,2],
        [4,5,4,5,8,1,2,4,3],
        [5,6,7,8,1,2,3,5,4],
        [6,7,8,1,2,3,4,6,5],
        [7,8,1,2,3,4,5,7,6]
    ])
    print(np.array([fLA(elem) for elem in ff]))

